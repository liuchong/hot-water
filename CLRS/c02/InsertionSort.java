import java.util.Arrays;

public class InsertionSort {
  public static <T extends Comparable<T>> int insertionSort(T[] inputArray) {
    int length = inputArray.length;
    for(int j = 1; j < length; j+=1) {
      T key = inputArray[j];
      int i = j - 1;
      while(i>=0 &&
            inputArray[i].compareTo(key) > 0) {
        inputArray[i+1] = inputArray[i];
        i-=1;
      }
      inputArray[i+1] = key;
    }
    return 0;
  }

  public static int insertionSort(int[] inputArray) {
    int length = inputArray.length;
    for(int j = 1; j < length; j+=1) {
      int key = inputArray[j];
      int i = j - 1;
      while(i>=0 &&
            inputArray[i] > key) {
        inputArray[i+1] = inputArray[i];
        i-=1;
      }
      inputArray[i+1] = key;
    }
    return 0;
  }

  public static void main(String [] argv) {
    Integer[] a = {5, 2, 4, 6, 1, 3};
    Integer[] a1 = {1, 2, 3, 4, 5, 6};
    insertionSort(a);
    // java -ea
    assert Arrays.equals(a, a1);
    int [] b = {0};
    int [] b1 = {0};
    insertionSort(b);
    assert Arrays.equals(b, b1);
  }
}
