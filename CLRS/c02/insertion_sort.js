/**
 * Sort the given array in place, will not use the high level build-in functions
 *
 * @example
 *   const a = [5, 2, 4, 6, 1, 3]
 *   insertionSort(a)
 *   a.toString()
 *   // => '1,2,3,4,5,6'
 */
function insertionSort (a) {
  const length = a.length
  for (let j = 1; j < length; j += 1) {
    const key = a[j]
    let i = j - 1
    while (i >= 0 && a[i] > key) {
      a[i + 1] = a[i]
      i = i - 1
    }
    a[i + 1] = key
  }
}

module.exports = insertionSort

if (!module.parent) {
  const assert = require('assert')
  const a = [5, 2, 4, 6, 1, 3]
  insertionSort(a)
  assert(a.toString() === [1, 2, 3, 4, 5, 6].toString())
  const b = [0]
  insertionSort(b)
  assert(b.toString() === [0].toString())
}
