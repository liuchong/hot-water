from array import array


def insertion_sort(a: array):
    """ Sort the given array in place, will not use the high level build-in
    functions.

    >>> a = array('i', [5, 2, 4, 6, 1, 3])
    >>> print(a)
    array('i', [5, 2, 4, 6, 1, 3])
    >>> insertion_sort(a)
    >>> print(a)
    array('i', [1, 2, 3, 4, 5, 6])
    >>> b = array('i', [0])
    >>> print(b)
    array('i', [0])
    >>> insertion_sort(b)
    >>> print(b)
    array('i', [0])
    """
    length = len(a)
    j = 1
    while j < length:
        key = a[j]
        i = j - 1
        while i >= 0 and a[i] > key:
            a[i + 1] = a[i]
            i = i - 1
        a[i + 1] = key
        j = j + 1


if __name__ == "__main__":
    import doctest
    doctest.testmod()
