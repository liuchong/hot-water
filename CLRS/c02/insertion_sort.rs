use std::cmp::PartialOrd;
use std::marker::Copy;

/// Insertion sort an array in-place.
pub fn insertion_sort<T: PartialOrd + Copy>(a: &mut [T]) {
    let length: usize = a.len();
    for j in 1..length {
        let key = a[j];
        let mut i = j;
        while i > 0 && a[i - 1] > key {
            a[i] = a[i - 1];
            i = i - 1;
        }
        a[i] = key;
    }
}

fn main() {
    use std::cmp::Ordering::Equal;
    let mut a = [5, 2, 4, 6, 1, 3];
    insertion_sort(&mut a);
    assert_eq!(a.partial_cmp(&[1, 2, 3, 4, 5, 6]), Some(Equal));
    let mut b = [0];
    insertion_sort(&mut b);
    assert_eq!(b.partial_cmp(&[0]), Some(Equal));
}
