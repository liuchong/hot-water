#include <stdio.h>

void print_array(int *a, int len) {
  int i;
  // int len = sizeof(a);
  for (i = 0; i < len; i += 1) {
    printf("%d, ", a[i]);
  }
  printf("\n");
}

void insertion_sort(int a[], int len) {
  int j;
  for (j = 1; j < len; j += 1) {
    int key = a[j];
    int i = j - 1;
    while (i >= 0 && a[i] > key) {
      a[i + 1] = a[i];
      i = i - 1;
    }
    a[i + 1] = key;
  }
}

int main(void) {
  int al = 6;
  int a[] = {5, 2, 4, 6, 1, 3};
  print_array(a, al);
  insertion_sort(a, al);
  print_array(a, al);

  int bl = 1;
  int b[] = {0};
  print_array(b, bl);
  insertion_sort(b, bl);
  print_array(b, bl);
}
