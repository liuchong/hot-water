object InsertionSort {

  def insertionSort[T <% Ordered[T]]( inputArray: Array[T]) {
    val length = inputArray.length
    var j = 1
    while (j < length) {
      val key = inputArray(j)
      var i = j - 1
      while(i >= 0 && inputArray(i) > key) {
        inputArray(i + 1) = inputArray(i)
        i = i - 1
      }
      inputArray(i + 1) = key
      j += 1
    }
  }

  def main(args: Array[String]) {
    val a: Array[Int] = Array(5, 2, 4, 6, 1, 3)
    insertionSort(a)
    // assert(a.mkString("") == "123456")
    assert(a.deep == Array[Int](1, 2, 3, 4, 5, 6).deep)
    val b: Array[Double] = Array(0)
    insertionSort(b)
    assert(b.deep == Array[Double](0).deep)
  }
}
